# dbus_notify - Popup notifications via dbus

This is a simple (very simple) script to generate notifications via dbus.


# Motivation

After the latest KDE updates I think that 5.14+ kdialog started showing a 'kdialog' instead of the header, it irritated me, so I looked for another way to generate it.

![exeples](https://github.com/luizoti/dbus_notify/blob/master/kdiad.png)

# Notify2

My script is based on [notify2](https://pypi.org/project/notify2/), it is probably more complete, my goal was to do something for my simple project, with the intention of learning too.

# How to use?

You can call this script via terminal or import it into another project.

Running via terminal:

```
python3 ./dbus_notify.py -t 'Header' -s 'Its a Summary' -m 'Here's your message, be happy' -i thunar -d 1

```

or you can give execution permissions with *chmod +x* and copy to */usr/bin/* as *dbus_notify* and run directly.


Import into another project by placing it in the same directory:

```
from dbus_notify import NotifyAction

if __name__ == '__main__':
    notify = NotifyAction()
    notify.create('Header', 'Its a Summary', 'Its a Summary', 'Here's your message, be happy', thunar, 1)
    pass
```
