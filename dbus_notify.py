#!/usr/bin/python3

import dbus
import argparse
from random import randrange
from dbus.mainloop.glib import DBusGMainLoop

parse = argparse.ArgumentParser(description='Create folder from selected files:')

parse.add_argument('-t', '--title', metavar='appname', help='Notification Title')
parse.add_argument('-s', '--summary', metavar='summary', help='Notification Summary')
parse.add_argument('-m', '--messege', metavar='messege', help='Notification Messege')
parse.add_argument('-i', '--icon', metavar='icon', help='Notification Icon')
parse.add_argument('-d', '--time', type=int, metavar='time', help='Notification timeout')
args = parse.parse_args()

# # 
try:
    if args.title:
        appname = args.title
    if args.summary:
        summary = args.summary
    if args.messege:
        messege = args.messege
    if args.icon:
        icon = args.icon
    if args.time:
        timeout = round(args.time * 1000)
except Exception as e:
    raise e

_id = randrange(999)
actions = ''
hints = ''

class NotifyAction(object):
    def __init__(self):
        maintloop = DBusGMainLoop()
        self.dbus_session = dbus.SessionBus(maintloop).get_object('org.freedesktop.Notifications', 
                                                                '/org/freedesktop/Notifications')

        pass


    def create(self, *args):
        dbus.Interface(self.dbus_session, 
            'org.freedesktop.Notifications'
                ).Notify(appname, _id, icon, summary, messege, actions, hints, timeout)
        pass
    pass


if __name__ == '__main__':
    notify = NotifyAction()
    notify.create(appname, summary, messege, icon, timeout)
    pass